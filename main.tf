variable "google_project" {}
variable "google_region" {}
variable "google_zone" {}

provider "google" {
  project = "${var.google_project}"
  region  = "${var.google_region}"
}

resource "google_compute_instance" "instance" {
  name         = "nat-test"
  machine_type = "f1-micro"
  zone         = "${var.google_zone}"

  boot_disk {
    initialize_params {
      image = "${data.google_compute_image.ubuntu.self_link}"
      size  = 10
      type  = "pd-standard"
    }
  }

  network_interface {
    network = "default"
  }

  scheduling {
    automatic_restart = false
    preemptible       = true
  }
}

resource "google_compute_instance" "bastion" {
  name         = "bastion"
  machine_type = "f1-micro"
  zone         = "${var.google_zone}"

  boot_disk {
    initialize_params {
      image = "${data.google_compute_image.ubuntu.self_link}"
      size  = 10
      type  = "pd-standard"
    }
  }

  network_interface {
    # Gives public IP
    access_config {}

    network = "default"
  }

  scheduling {
    automatic_restart = false
    preemptible       = true
  }
}

data "google_compute_image" "ubuntu" {
  family  = "ubuntu-1804-lts"
  project = "ubuntu-os-cloud"
}

module "nat" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/cloud-nat.git?ref=craigf/initial-implementation"

  nat_ip_count     = 2
  nat_ports_per_vm = 128
  network_name     = "default"
  region           = "${var.google_region}"
}

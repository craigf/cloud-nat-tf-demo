# Cloud NAT Terraform demo

1. Copy these instructions so that they don't get clobbered by the git commands
   (or look at them on the website).
1. `git checkout before-nat && terraform apply`
1. I've included my `.envrc` as an example. Fellow GitLab employees, please
   don't provision these resources in my sandbox project. Edit the values in the
   .envrc and source it (or automate this with
   [`direnv`](https://github.com/direnv/direnv)).
1. `gcloud compute --project <project> ssh --zone <zone> nat-test`, then copy
   your ssh public key into `~/.ssh/authorized_keys` to set up quick-and-dirty
   bastion ssh ability.
1. `gcloud compute --project <project> ssh --zone <zone> bastion -- -A`, then
   `ssh <private-ip-of-nat-test-vm>`. This will allow you to keep an ssh session
   open while removing the public IP of the VM.
1. Check the apparent public IP of the VM with `curl https://api.ipify.org`. It
   should be the VMs own public IP.
1. `git checkout provision-nat && terraform apply`. This will also remove the
   public IP of the VM.
1. Check the apparent public IP of the VM again. It should now be the NAT
   gateway static IP.
1. Subsequent commits on master demonstrate scaling out the NAT IPs, and the
   minimum ports per VM.
